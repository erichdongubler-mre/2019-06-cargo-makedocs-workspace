#! /bin/sh

# `cargo-makedocs` looks like an interesting tool, but it doesn't seem to work
# with Cargo workspaces yet.
#
# Steps to reproduce:
#
# 1. Create a new workspace with a single member -- for the sake of example,
# let's call this `binary` and make it a new Cargo binary.
dir="$(CDPATH= cd -- "$(dirname -- "$0")" && pwd -P)"
cd "$dir"
NEW_WORKSPACE_NAME="new-workspace"
NEW_MEMBER_NAME="binary"
if [ -e "$NEW_WORKSPACE_NAME" ]; then
	rm -rf "$NEW_WORKSPACE_NAME" || exit $?
fi
cargo new "$NEW_WORKSPACE_NAME" || exit $?
cd "$NEW_WORKSPACE_NAME" || exit $?
rm -rf *
cat > Cargo.toml <<-EOF || exit $?
	[workspace]
	members = ['$NEW_MEMBER_NAME']
EOF
cargo new --bin $NEW_MEMBER_NAME || exit $?

# 2. Build the workspace. This shouldn't fail.
cargo build || exit $?

# 3. Sanity check: run `cargo makedocs` on workspace root, expecting it to fail
# because a concrete workspace member should be used to generate docs.
TEMP_CARGO_INSTALL_ROOT="$dir/cargo-install-root"
if [ -e "$TEMP_CARGO_INSTALL_ROOT" ]; then
	rm -rf "$TEMP_CARGO_INSTALL_ROOT" || exit $?
fi
mkdir "$TEMP_CARGO_INSTALL_ROOT" || exit $?
export PATH="$PATH:$TEMP_CARGO_INSTALL_ROOT/bin"
cargo install --root "$TEMP_CARGO_INSTALL_ROOT" cargo-makedocs || exit $?
cargo makedocs && { echo "expected \`cargo makedocs\` to fail at workspace root!"; exit 1; }
echo "^ This failure is expected -- it's the workspace root. :)"

# 4. Change directory into the newly built `binary` workspace member. Let's try
# to use `makedocs` on it, expecting this to work.
cd "$NEW_MEMBER_NAME"
cargo makedocs || exit $?

# Unfortunately, the above invocation fails, complaining that `Cargo.lock`
# couldn't be found. `Cargo.lock` DOES exist, but at the workspace root and not
# this working directory. This seems to indicate that `makedocs` simply isn't
# aware of workspaces yet.
